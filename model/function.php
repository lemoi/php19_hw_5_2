<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
ini_set('display_errors', 1);

session_start();

$config = require_once 'config.php';

require_once 'lib/database/DataBase.php';
$db = DataBase::connect(
    $config['mysql']['host'],
    $config['mysql']['dbname'],
    $config['mysql']['user'],
    $config['mysql']['pass']
);

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./template');
$twig = new Twig_Environment($loader, [
    'cache' => './tmp/cache',
    'auto_reload' => true
]);

function getParamGet($name)
{
    return array_key_exists($name, $_GET) ? htmlspecialchars($_GET[$name]) : null;
}

function getParamPost($name)
{
    return array_key_exists($name, $_POST) ? htmlspecialchars($_POST[$name]) : null;
}

function getParamSession($name)
{
    return array_key_exists($name, $_SESSION) ? $_SESSION[$name] : null;
}

function isPost()
{
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function redirect($page)
{
    header('Location: ' . $page);
}

function logout()
{
    session_destroy();
}

function render($template, $params = [])
{
    $fileTemplate = 'template/' . $template;
    if (is_file($fileTemplate)) {
        ob_start();
        if (count($params) > 0) {
            extract($params);
        }
        require $fileTemplate;
        return ob_get_clean();
    }
}

function getPdo()
{
    /*$opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    //$pdo = new PDO("mysql:host=localhost;charset=utf8;dbname=global", "lmoiseev", "neto1490");
    $pdo = new PDO("mysql:host=localhost;port=3306;charset=utf8;dbname=global", "root", "root", $opt);

    // эмуляция разрешит использовать многократное использование одного именнованного параметра в одном запросе, хотя возможно сделает код уязвимым к определенного рода инъекционным атакам
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE); // используется в методе getTasks

    return $pdo;
    */

    global $db;
    return $db;
}

function createTask($description)
{
    $pdo = getPdo();
    $stmt = $pdo->prepare("INSERT INTO `task` (`description`, `date_added`, `user_id`) VALUES (:desc, NOW(), :userId)");
    $stmt->execute(['desc' => $description, 'userId' => getParamSession('userId')]);
}

function deleteTask($id)
{
    $pdo = getPdo();
    $stmt = $pdo->prepare("DELETE FROM `task` WHERE `id` = :id");
    $stmt->execute(['id' => $id]);
}

function getTask($id)
{
    $pdo = getPdo();
    $stmt = $pdo->prepare("SELECT * FROM `task` WHERE `id` = :id");
    $stmt->execute(['id' => $id]);

    return $stmt->fetch();
}

function completeTask($id)
{
    $pdo = getPdo();
    $stmt = $pdo->prepare("UPDATE `task` SET `is_done` = TRUE WHERE `id` = :id");
    $stmt->execute(['id' => $id]);
}

function resumeTask($id)
{
    $pdo = getPdo();
    $stmt = $pdo->prepare("UPDATE `task` SET `is_done` = FALSE WHERE `id` = :id");
    $stmt->execute(['id' => $id]);
}

function updateTask($id, $description)
{
    $pdo = getPdo();
    $stmt = $pdo->prepare("UPDATE `task` SET `description` = :desc WHERE `id` = :id");
    $stmt->execute(['id' => $id, 'desc' => $description]);
}

function getTasks($order, $category = null)
{
    $pdo = getPdo();

    $orders = ['date_added', 'is_done', 'description'];
    $key = array_search($order, $orders);
    if ($key === false) {
        $order = $orders[0];
    }

    if ($category == 'own') {
        $where = 'A.`user_id` = :userId';
    } elseif ($category == 'assigned') {
        $where = 'A.`assigned_user_id` = :userId AND A.`user_id` <> :userId';
    } else {
        $where = 'A.`user_id` = :userId OR TRUE';
    }

//$stmt = $pdo->prepare("SELECT *, NULL AS `responsible`, NULL AS `author` FROM `task` WHERE `user_id` = :userId ORDER BY `$order` ASC");
    $stmt = $pdo->prepare("
SELECT 
  A.`id`, A.`description`, A.`is_done`, A.`date_added`, A.`user_id`, A.`assigned_user_id`, B.`login` AS `responsible`, C.`login` AS `author`  
FROM `task` AS A 
  LEFT JOIN `user` AS B ON A.`assigned_user_id` = B.`id` 
  LEFT JOIN `user` AS C ON A.`user_id` = C.`id` 
WHERE $where  
ORDER BY A.`$order` ASC");
    $userId = getParamSession('userId');
    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
    $stmt->execute();
    //$stmt->execute(['userId' => getParamSession('userId')]);

    return $stmt;
}

function userExists($login)
{
    $pdo = getPdo();

    $stmt = $pdo->prepare("SELECT COUNT(*) FROM `user` WHERE `login` = :name");
    $stmt->execute(['name' => trim($login)]);

    return $stmt->fetchColumn() != 0;
}

function getUser($login, $password)
{
    $pdo = getPdo();

    $stmt = $pdo->prepare("SELECT * FROM `user` WHERE `login` = :name AND `password` = :password");
    $stmt->execute(['name' => trim($login), 'password' => md5(trim($password))]);

    return $stmt->fetch();
}

function getUsers()
{
    $pdo = getPdo();
    $result = $pdo->query("SELECT `login`, `id` FROM `user`");
    return $result->fetchAll();
}

function createUser($login, $password)
{
    $pdo = getPdo();

    if (userExists($login)) {
        $result = false;
    } else {
        $stmt = $pdo->prepare("INSERT INTO `user` (`login`, `password`) VALUES (:login, :password)");
        $result = $stmt->execute(['login' => $login, 'password' => md5(trim($password))]);

        if ($result == true) {
            $result = getUser($login, $password);
        }
    }

    return $result;
}

function assignTask($taskId, $userId)
{
    $pdo = getPdo();
    $stmt = $pdo->prepare("UPDATE `task` SET `assigned_user_id` = :userId WHERE `id` = :taskId");
    $stmt->execute(['taskId' => $taskId, 'userId' => $userId]);
}
