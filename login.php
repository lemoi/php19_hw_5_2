<?php

require_once 'model/function.php';

$message = 'Введите данные для регистрации или войдите, если уже регистрировались:';
if (isPost()) {
    $login = trim(getParamPost('login'));
    $password = trim(getParamPost('password'));

    $register = array_key_exists('register', $_POST);
    $sign_in = array_key_exists('sign_in', $_POST);
    if ($register || $sign_in) {
        if (empty($login) || empty($password)) {
            $message = 'Поля логин и пароль обязательны для заполнения.';

        } elseif ($register) {
            $result = createUser($login, $password);
            if ($result == true) {
                $_SESSION['userId'] = $result['id'];
                $message = '';
            } else {
                $message = 'Такой пользователь уже существует в базе данных.';
            }

        } elseif ($sign_in) {
            $result = getUser($login, $password);
            if ($result == false) {
                $message = 'Такой пользователь не существует, либо неверный пароль.';
            } else {
                $_SESSION['userId'] = $result['id'];
                $message = '';
            }
        }
    }
}

if (!empty(getParamSession('userId'))) {
    redirect('list.php');
    die;
} else {
    $params = ['user' => ['login' => $login], 'message' => $message];
    //echo render('login.twig', $params);
    echo $twig->render('login.twig', $params);
}
?>
