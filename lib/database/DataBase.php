<?php

class DataBase
{
    /**
     * Подключение к базе данных mysql
     * @param $host адрес
     * @param $dbname название базы
     * @param $user пользователь
     * @param $pass пароль
     */
    public static function connect($host, $dbname, $user, $pass)
    {
        try {
            $opt = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => false,
            ];
            $db = new PDO('mysql:host=' . $host . '; port=3306; dbname=' . $dbname . '; charset=utf8', $user, $pass, $opt);

            // эмуляция разрешит использовать многократное использование одного именнованного параметра в одном запросе, хотя возможно сделает код уязвимым к определенного рода инъекционным атакам
            $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE); // используется в методе getTasks

        } catch (PDOException $e) {
            die('Database error: '.$e->getMessage().'<br/>');
        }

        return $db;
    }
}