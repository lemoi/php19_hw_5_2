<?php
require_once 'model/function.php';

$users = getUsers();

$action = getParamGet('action');
$id = (int)getParamGet('id');
$description = '';

if ($action == 'delete') {
    deleteTask($id);
} elseif ($action == 'edit') {
    $task = getTask($id);
    $description = $task['description'];
    $_SESSION['taskId'] = $task['id'];
} elseif ($action == 'done') {
    $task = getTask($id);
    if ($task['is_done']) {
        resumeTask($id);
    } else {
        completeTask($id);
    }
    redirect('index.php');
}

$order = 'date_added';
if (isPost()) {
    if (array_key_exists('save', $_POST) && !empty(getParamPost('description'))) {
        if (!empty($id) && getParamPost('save') == 'Сохранить') {
            updateTask($id, getParamPost('description'));
            unset($_SESSION['taskId']);
            redirect('index.php');

        } elseif (getParamPost('save') == 'Добавить') {
            createTask(getParamPost('description'));
        }

    } elseif (array_key_exists('sort', $_POST)) {
        $order = getParamPost('sort_by');

    } elseif (array_key_exists('assign', $_POST)) {
        $arg = explode('_', getParamPost('assigned_user_id'));
        if (count($arg) == 4) {
            assignTask($arg[3], $arg[1]);
        }
    }
}

//$result = getTasks($order);
$result = getTasks($order, 'own');
$userTask = $result == false ? [] : $result;

$result = getTasks($order, 'assigned');
$userAssigned = $result == false ? [] : $result;

$currentAction = getParamSession('taskId') == null ? 'Добавить' : 'Сохранить';

$selectOrder = [];
foreach (['date_added', 'is_done', 'description'] as $typeOrder) {
    $selectOrder[$typeOrder] = $order == $typeOrder ? 'selected ' : '';
}

$params = [
    'currentUser' => ['id' => getParamSession('userId')],
    'users' => $users,
    'selectOrder' => $selectOrder,
    'description' => $description,
    'userTask' => $userTask,
    'userAssigned' => $userAssigned,
    'currentAction' => $currentAction
];
//echo render('list.twig', $params);
echo $twig->render('list.twig', $params);

?>